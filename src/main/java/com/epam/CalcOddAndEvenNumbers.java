package com.epam;


/**
 * Class for selecting from the interval odd and even numbers.
 * Class also calculate sum of odd and sum of even numbers from the interval
 */
public class CalcOddAndEvenNumbers {
    /**
     * The input interval.
     */
    private int[] inputInterval;
    /**
     * Array of odd numbers.
     */
    private int[] oddNum;
    /**
     * Array of even numbers.
     */
    private int[] evenNum;
    /**
     * Sum of odd numbers.
     */
    private int   sumOdd;
    /**
     * Sum of even numbers.
     */
    private int   sumEven;
    /**
     * Constructor of class CalcOddAndEvenNumbers.
     * @param interval interval for calculation.
     */
    CalcOddAndEvenNumbers(final int[] interval) {
        if (interval[0] < interval[1]) {
            this.inputInterval = new int[]{interval[0], interval[1]};
        } else {
            this.inputInterval = new int[]{interval[1], interval[0]};
        }
        int length = (this.inputInterval[1] - this.inputInterval[0]) / 2;
        if (this.inputInterval[0] % 2 != this.inputInterval[1] % 2) {
            length++;
            oddNum = new int[length];
            evenNum = new int[length];
        } else if (this.inputInterval[0] % 2 != 0) {
            oddNum = new int[length + 1];
            evenNum = new int[length];
        } else {
            oddNum = new int[length];
            evenNum = new int[length + 1];
        }
        sumOdd =  0;
        sumEven = 0;
        for (int i = this.inputInterval[0], iOdd = 0, iEven = 0;
             i <= this.inputInterval[1]; i++) {
            if (i % 2 != 0) {
                oddNum[iOdd] = i;
                sumOdd += i;
                iOdd++;
            } else {
                evenNum[iEven] = i;
                sumEven += i;
                iEven++;
            }
        }
    }
    /**
     * Method for return the interval.
     * @return int[] This returns input interval
     */
    final int[] getInputInterval() {
        return inputInterval.clone();
    }
    /**
     * Method for return array of even numbers.
     * @return int[] This returns array of even numbers
     */
    final int[] getEvenNum() {
        return evenNum.clone();
    }
    /**
     * Method for return array of odd numbers.
     * @return int[] This returns array of odd numbers
     */
    final int[] getOddNum() {
        return oddNum.clone();
    }
    /**
     * Method for return sum of even numbers.
     * @return int This returns sum of even numbers
     */
    final int getSumEven() {
        return sumEven;
    }
    /**
     * Method for return sum of odd numbers.
     * @return int This returns sum of odd numbers
     */
    final int getSumOdd() {
        return sumOdd;
    }

}
