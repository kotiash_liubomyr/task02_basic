package com.epam;

import java.util.Scanner;

/**
 * The main class of the application.
 */
final class Application {
    /**
     * private constructor without arguments.
     */
    private Application() {

    }
    /**
     * The main method of the application.
     * @param args Unused
     */
    public static void main(final String[] args) {
        Scanner in = new Scanner(System.in, "UTF-8");
        System.out.println("Enter the interval (for example: [1;100]):");
        String input = in.nextLine();
        int[] interval = getInterval(input);
        CalcOddAndEvenNumbers numbs = new CalcOddAndEvenNumbers(interval);
        System.out.println("Odd numbers from " + interval[0]
                + " to " + interval[1] + ":");
        if (interval[0] > interval[1]) {
            printIntArray(numbs.getOddNum(), true);
            System.out.println("Even numbers from " + interval[1]
                    + " to " + interval[0] + ":");
            printIntArray(numbs.getEvenNum());
        } else {
            printIntArray(numbs.getOddNum());
            System.out.println("Even numbers from " + interval[1]
                    + " to " + interval[0] + ":");
            printIntArray(numbs.getEvenNum(), true);
        }
        System.out.println("Sum of odd numbers: " + numbs.getSumOdd());
        System.out.println("Sum of even numbers: " + numbs.getSumEven());
        System.out.println("Enter size of set of Fibonacci numbers:");
        int numberOfFibonacciNumbers = in.nextInt();
        FibonacciNumbersCalcOddAndEven fibonacciNumbs =
                new FibonacciNumbersCalcOddAndEven(numberOfFibonacciNumbers,
                        numbs.getOddNum()[numbs.getOddNum().length - 1],
                        numbs.getEvenNum()[numbs.getEvenNum().length - 1]);
        System.out.println("Percentage of odd Fibonacci numbers: "
                + fibonacciNumbs.getPercentageOfOddNumbers());
        System.out.println("Percentage of even Fibonacci numbers: "
                + fibonacciNumbs.getPercentageOfEvenNumbers());
    }
    /**
     * Method for return an integer number from a string.
     * @param str string with int number
     * @param interval interval int number in str
     * @return int number
     */
    public static int getInt(final String str, final int[] interval) {
        return Integer.parseInt(str.substring(interval[0], interval[1]).trim());
    }
    /**
     * Method of returning an interval of the format: [begin;end] from a string.
     * @param str string with interval by format: [begin;end]
     * @return array of two elements(first - start of interval;
     * second - finish of interval)
     */
    public static int[] getInterval(final String str) {
        int[] result = new int[2];
        int iEndFirstValue = 0x0;
        for (int i = 0, size = str.length(); i < size; i++) {
            if (str.charAt(i) == ';') {
                iEndFirstValue = i;
                break;
            }
        }
        result[0] = getInt(str, new int[]{1, iEndFirstValue});
        result[1] = getInt(str, new int[]{iEndFirstValue + 1,
                str.length() - 1});
        return result;
    }
    /**
     * Method for print array to console(overload
     * to static void printIntArray(int[] array, boolean reverse)).
     * @param array array to output
     */
    public static void printIntArray(final int[] array) {
        printIntArray(array, false);
    }
    /**
     * Method for print array to console(reversible or non-reversible).
     * @param array array to output
     * @param reverse if reverse equals true output reverse array
     */
    public static void printIntArray(final int[] array, final boolean reverse) {
        int begin;
        int end;
        int inc;
        if (reverse) {
            begin = array.length - 1;
            end = -1;
            inc = -1;
        } else {
            begin = 0;
            end = array.length;
            inc = 1;
        }
        for (int i = begin; i != end; i += inc) {
            System.out.print(array[i] + " ");
        }
        System.out.print("\n");
    }
}
