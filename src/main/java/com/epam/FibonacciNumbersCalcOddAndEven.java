package com.epam;
/**
 * Class for calculate percentage of odd and percentage of even
 * Fibonacci numbers with F1 and F2.
 */
public class FibonacciNumbersCalcOddAndEven {
    /**
     * Percentage of add Fibonacci numbers.
     */
    private double percentageOfOddNumbers;
    /**
     * Percentage of even Fibonacci numbers.
     */
    private double percentageOfEvenNumbers;
    /**
     * Max number of percentage.
     */
    private static final int PERCENTAGE = 100;
    /**
     * Constructor of class CalcOddAndEvenNumbers.
     * @param numberOfFibonacciNumbers number of Fibonacci numbers
     * @param fibonacci1 first Fibonacci number
     * @param fibonacci2 second Fibonacci number
     */
    FibonacciNumbersCalcOddAndEven(final int numberOfFibonacciNumbers,
                                   final int fibonacci1, final int fibonacci2) {
        int f1 = fibonacci1;
        int f2 = fibonacci2;
        percentageOfOddNumbers = 0;
        percentageOfEvenNumbers = 0;
        for (int i = 0; i < numberOfFibonacciNumbers; i++) {
            int tempFibonacci = f1 + f2;
            if (tempFibonacci % 2 != 0) {
                percentageOfOddNumbers++;
            } else {
                percentageOfEvenNumbers++;
            }
            f1 = f2;
            f2 = tempFibonacci;
        }
        percentageOfOddNumbers /= numberOfFibonacciNumbers;
        percentageOfEvenNumbers /= numberOfFibonacciNumbers;
        percentageOfOddNumbers *= PERCENTAGE;
        percentageOfEvenNumbers *= PERCENTAGE;
    }
    /**
     * Method for return the percentage of add Fibonacci numbers.
     * @return double This returns the percentage of add Fibonacci numbers
     */
    final double getPercentageOfOddNumbers() {
        return percentageOfOddNumbers;
    }
    /**
     * Method for return the percentage of even Fibonacci numbers.
     * @return double This returns the percentage of even Fibonacci numbers
     */
    final double getPercentageOfEvenNumbers() {
        return percentageOfEvenNumbers;
    }
}
